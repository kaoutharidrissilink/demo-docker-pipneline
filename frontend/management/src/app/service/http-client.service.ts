import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../employee';


@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor(private httpClient:HttpClient) { }

  
  getEmployees()
  {
    console.log("test call");
    return this.httpClient.get<Employee[]>('http://localhost:8081/api/test/deploy/students-list');
  }

  
  public deleteEmployee(employee: Employee) {
    return this.httpClient.delete<Employee>("http://localhost:8081/api/test/deploy/delete-student" + "/"+ employee.id);
  }

  public createEmployee(employee: Employee) {
    return this.httpClient.post<Employee>("http://localhost:8081/api/test/deploy/save/student", employee);
  }
}
