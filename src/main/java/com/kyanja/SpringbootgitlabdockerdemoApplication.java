package com.kyanja;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
public class SpringbootgitlabdockerdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootgitlabdockerdemoApplication.class, args);
    }

}
