package com.kyanja.controller;

import com.kyanja.model.Student;
import com.kyanja.service.StudentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/test/deploy")
@CrossOrigin(origins = "http://localhost:4200")
public class StudentController {

    @Autowired
    StudentServiceImpl studentService;



    @GetMapping("/student")
    @ResponseBody
    public ResponseEntity<Student> getStudentbyName(@RequestParam String name) {

        Student student;

        try {

            student  = studentService.getStudentByName(name);

        } catch (Exception e) {

            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(student);

    }

    @PostMapping("/save/student")
    public ResponseEntity<Student> createStudent(@RequestBody Student student) {
        try {
            Optional<Student>  myStudent = Optional.ofNullable(studentService
                    .addStudent(student));
            return new ResponseEntity(myStudent, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @DeleteMapping("/delete-student/{student_id}")
    public  ResponseEntity<Student> deleteStudent(@PathVariable("student_id") Long student_id,Student student) {
        student.setId(student_id);
        studentService.deleteStudent(student);
        return new ResponseEntity(student, HttpStatus.OK);
    }

    @PostMapping("/update-student/{student_id}")
    public ResponseEntity<Student>  updateStudent(@RequestBody Student student,@PathVariable("student_id") Long student_id) {
        student.setId(student_id);
        studentService.updateStudent(student);
        return new ResponseEntity(student, HttpStatus.OK);

    }

    @GetMapping("/students-list")
    ResponseEntity<List<Student>> getAllStudents() {
      return ResponseEntity.ok().body(studentService.getAllStudents());
    }




}
