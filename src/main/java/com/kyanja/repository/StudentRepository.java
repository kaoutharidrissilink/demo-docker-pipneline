package com.kyanja.repository;

import com.kyanja.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Long> {

    Student findByName(String name);

    List<Student> findAllByName(String name);

}
