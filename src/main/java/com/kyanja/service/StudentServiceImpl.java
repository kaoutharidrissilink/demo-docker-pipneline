package com.kyanja.service;


import com.kyanja.model.Student;
import com.kyanja.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl {


    @Autowired
    StudentRepository studentRepository;


    public Student addStudent(Student student) {

        try {
            studentRepository.save(student);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return student;
    }


    public Student getStudentByName(String name) {

        return studentRepository.findByName(name);
    }

    public List<Student> getAllStudents() {

        return studentRepository.findAll();
    }

    public boolean deleteStudent(Student student) {

        boolean status=false;
        try {
            studentRepository.delete(student);
            status=true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public boolean updateStudent(Student student) {

        boolean status=false;

        try {
            studentRepository.saveAndFlush(student);
            status=true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }


}
